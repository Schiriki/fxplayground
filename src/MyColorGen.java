import java.awt.Color;
public class MyColorGen {
	public MyColorGen(){
	}
	public static Color Gen() {
		int r = (int) (Math.random() * 255);
		int g = (int) (Math.random() * 255);
		int b = (int) (Math.random() * 255);
		Color temp = new Color(r,g,b);
		return temp;
	}
	
}
