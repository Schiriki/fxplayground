import java.awt.Button;
import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * 
 * @author Kolja Gorter
 * @version 0.3
 *
 */

public class MainClass {
	
	public static void main(String[] args) {
		
		System.out.println("Kolja Gorter!");
		Color bgc = new Color(255, 102, 0);
		Button b1 = new Button("random bg color");
		b1.setBackground(Color.red);
		b1.setBounds(20,50,120,30);
		Button b2 = new Button("Reset bg color");
		b2.setBackground(Color.yellow);
		b2.setBounds(20,90,100,30);
		Button b3 = new Button("+");
		b3.setBackground(Color.cyan);
		b3.setBounds(59,220,30,20);
		Button b4 = new Button("-");
		b4.setBackground(Color.cyan);
		b4.setBounds(20,220,30,20);
		Button b5 = new Button("x");
		b5.setBackground(Color.cyan);
		b5.setBounds(20,192,30,20);
		Button b6 = new Button("/");
		b6.setBackground(Color.cyan);
		b6.setBounds(59,192,30,20);
		Button b7 = new Button("%");
		b7.setBackground(Color.cyan);
		b7.setBounds(20,164,30,20);
		
		final TextField tf1=new TextField();
		tf1.setBounds(100,140, 70,20);
		final TextField tf2=new TextField();
		tf2.setBounds(100,170, 70,20);
		final TextField tf3=new TextField();
		tf3.setBounds(100,220, 140,20);
		
		MyFrame fr = new MyFrame("Test frame");
		fr.add(b1);
		fr.add(b2);
		fr.add(b3);
		fr.add(b4);
		fr.add(b5);
		fr.add(b6);
		fr.add(b7);
		fr.setSize(260,250);
		fr.setBackground(bgc);
		fr.setLayout(null/*new FlowLayout()*/);
		fr.setVisible(true);
		fr.add(tf1);
		fr.add(tf2);
		fr.add(tf3);
		
		b1.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				fr.setBackground(MyColorGen.Gen());
			}
		});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				fr.setBackground(bgc);
			}
		});
		b3.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				String s1 = tf1.getText();
				String s2 = tf2.getText();
				int a = Integer.parseInt(s1);
				int b = Integer.parseInt(s2);
				int c = a + b;
				String result = String.valueOf(c);
				tf3.setText(result);
			}
		});
		b4.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				String s1 = tf1.getText();
				String s2 = tf2.getText();
				int a = Integer.parseInt(s1);
				int b = Integer.parseInt(s2);
				int c = a - b;
				String result = String.valueOf(c);
				tf3.setText(result);
			}
		});
		b5.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				String s1 = tf1.getText();
				String s2 = tf2.getText();
				int a = Integer.parseInt(s1);
				int b = Integer.parseInt(s2);
				int c = a * b;
				String result = String.valueOf(c);
				tf3.setText(result);
			}
		});
		b6.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				tf3.setText("To fucking coplicated");
			}
		});
		b7.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				String s1 = tf1.getText();
				String s2 = tf2.getText();
				int a = Integer.parseInt(s1);
				int b = Integer.parseInt(s2);
				int c = a % b;
				String result = String.valueOf(c);
				tf3.setText(result);
			}
		});
		
	}
}
